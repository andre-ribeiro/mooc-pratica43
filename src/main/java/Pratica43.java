
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author andre
 */
public class Pratica43 {
    public static void main(String[] args) {
        Retangulo r = new Retangulo(4.0,2.0);
        Quadrado q = new Quadrado(2.0);
        
        System.out.println(r.getArea());
        System.out.println(r.getPerimetro());
        
        System.out.println(q.getArea());
        System.out.println(q.getPerimetro());
        
        
    }
}
