/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author andre
 */
public class Retangulo implements FiguraComLados {
    protected double w, h;
    
    public Retangulo(double _w, double _h) {
        this.w = _w;
        this.h = _h;
    }

    @Override
    public double getLadoMenor() {
        return w < h? w : h;
    }

    @Override
    public double getLadoMaior() {
        return w > h? w : h;
    }
    
    @Override
    public String getNome() {
        return "Retangulo";
    }

    @Override
    public double getPerimetro() {
        return 2*w + 2*h;
    }

    @Override
    public double getArea() {
        return w*h;
    }
}
